var area1,area2,area3,area4,area5,area6,area7,area8,area9;
var app;
var db;
var magatzemSudoku;
var dadesSudoku;
var cronometre;
var hores,minuts,segons;
var buides = 36;


Vue.component('areas',{
    props:['nom','id','rowsudoku','colsudoku'],
    data: function(){return {valors: [{caselles:[]},{sudoku:[]}]}},
    methods:{
        afegirDades:function(dades){dades.forEach(dada => {(this.valors[0].caselles).push(dada);this.valors[1].sudoku.push(dada);})},
        eliminarRandom:function(){            
            let randomnums = new Set();
            while(randomnums.size < 4)            
                randomnums.add(Math.floor(Math.random()*(10-1)+1));
            randomnums.forEach(x => {let ind = (this.valors[0].caselles).indexOf(x);(this.valors[0]).caselles[ind] =" "} );            
        },
        pintaBackground:function(){
           let area = document.getElementById(this.id);
           for(let i = 0; i < area.rows.length;i++)
           {
               let row = area.children[i];
                for(cell = 0; cell < row.children.length;cell++)
                    row.children[cell].style.background = "chartreuse";
           }
        },
        pintarow:function(ev){     
            /* div exterior del sudoku */       
            let contenidor = ev.target.parentElement.parentElement.parentElement.parentElement;                                
            for(let i = 0; i < contenidor.children.length;i++)
            {
               /* div que conté cada taula */
               let divtaula = contenidor.children[i];
               /* taula de cada area */
               let taula = divtaula.children[0];
               for(j = 0; j < taula.rows.length;j++)     
                 /* Si la propietat rowsudoku ( fila de taules ) del component es igual al
                    atribut rowsudoku de la fila de la taula i el index de la fila de la taula
                    a la que pertany la casella del ratoli, es igual al index de la fila de la
                    taula actual, pintar la fila */                
                 if(this.rowsudoku == taula.children[j].getAttribute("rowsudoku") && ev.target.parentElement.rowIndex == j)
                 {
                    let row = taula.children[j];
                    for(let cell = 0; cell < row.children.length;cell++)
                        row.children[cell].style.background = "chartreuse";   
                 }                
            }
        },
        pintacol:function(ev){     
            /* div exterior del sudoku */       
            let contenidor = ev.target.parentElement.parentElement.parentElement.parentElement;                        
            for(let i = 0; i < contenidor.children.length;i++)
            {
               /* div que conté cada taula */
               let divtaula = contenidor.children[i];
               /* taula de cada area */
               let taula = divtaula.children[0];
               for(j = 0; j < taula.rows.length;j++){     
                 /* Si la propietat colsudoku ( columna de taules ) del component es igual al
                    atribut colsudoku de la fila de la taula i el index de la cela de la fila
                    a la que pertany la casella del ratoli, es igual al index de la cela de la
                    fila de la taula actual, pintar la cela */                
                   let row = taula.children[j];
                   for(cell= 0; cell < row.children.length; cell++)
                   {                       
                       if(this.colsudoku == taula.children[j].getAttribute("colsudoku") &&
                          ev.target.cellIndex === cell)
                            row.children[cell].style.background = "chartreuse";                                             
                   }
                }
            }
        },
        despintatot:function(ev){     
            /* div exterior del sudoku */       
            let contenidor = ev.target.parentElement.parentElement.parentElement.parentElement;                        
            for(let i = 0; i < contenidor.children.length;i++)
            {
               /* div que conté cada taula */
               let divtaula = contenidor.children[i];               
               /* taula de cada area */
               let taula = divtaula.children[0];           
               for(j = 0; j < taula.rows.length;j++){     
                 /* Si la propietat colsudoku ( columna de taules ) del component es igual al
                    atribut colsudoku de la fila de la taula i el index de la cela de la fila
                    a la que pertany la casella del ratoli, es igual al index de la cela de la
                    fila de la taula actual, pintar la cela */                
                   let row = taula.children[j];
                   for(cell= 0; cell < row.children.length; cell++)
                   {                                              
                     row.children[cell].style.background = "white";                                             
                   }
                }
            }
        },
        putfocus:function(ev){            
            let cela = ev.target;
            cela.setAttribute('contenteditable',true);
            cela.focus();
            cela.addEventListener('keyup',()=>{
                let table = cela.parentElement.parentElement;
                let row = cela.parentElement;
                let rowindex = row.rowIndex;
                let celaindex = cela.cellIndex;
                let valorindex = rowindex*3 + celaindex; 
                let valorsudoku = this.valors[1].sudoku[valorindex];
                let valorcela = ev.target.innerText;
                if(valorcela != valorsudoku)
                    ev.target.style.color="red";
                else{
                    ev.target.style.color="green";
                    buides--;
                    if(buides === 0)
                    {
                        clearInterval(cronometre);
                        this.ematgazemarTemps()                        
                    }
                }
            });            
        },
        ematgazemarTemps()
        {
            let actual = JSON.stringify({"hores":hores,"minuts":minuts,"segons":segons});
            let p1 = localStorage.getItem("temps1");
            let p2 = localStorage.getItem("temps2");
            let p3 = localStorage.getItem("temps3");
            if(p1 === null)
                localStorage.setItem("temps1",actual);
            else 
            {
                let t1 = JSON.parse(p1);
                if(t1.hores > hores || (t1.hores === hores && t1.minuts > minuts) || 
                  (t1.hores === hores && t1.minuts === minuts && t1.segons > segons))
                    localStorage.setItem("temps1",actual);
                else if(p2 === null)
                    localStorage.setItem("temps2",actual);
                else
                {
                    let t2 = JSON.parse(p2);
                    if(t2.hores > hores || (t2.hores === hores && t2.minuts > minuts) || 
                      (t2.hores === hores && t2.minuts === minuts && t2.segons > segons))
                    localStorage.setItem("temps2",actual);
                    else if(p3 === null)
                        localStorage.setItem("temps3",actual);
                    else
                    {
                        let t3 = JSON.parse(p3);
                        if(t3.hores > hores || (t3.hores === hores && t3.minuts > minuts) || 
                          (t3.hores === hores && t3.minuts === minuts && t3.segons > segons))
                        localStorage.setItem("temps3",actual);                        
                    }
                }
            }
        }
    },
    template:`
    <div id="divtaula">    
        <table v-bind:id="id" contenteditable="false" >
            <tr v-bind:rowsudoku="rowsudoku" v-bind:colsudoku="colsudoku"><td @click="putfocus" v-on:mouseover="pintarow" v-on:mouseover="pintacol" v-on:mouseover="pintaBackground" v-on:mouseleave="despintatot">{{valors[0].caselles[0]}}</td><td @click="putfocus" v-on:mouseover="pintarow" v-on:mouseover="pintacol" v-on:mouseover="pintaBackground" v-on:mouseleave="despintatot">{{valors[0].caselles[1]}}</td><td @click="putfocus" v-on:mouseover="pintarow" v-on:mouseover="pintacol" v-on:mouseover="pintaBackground" v-on:mouseleave="despintatot">{{valors[0].caselles[2]}}</td></tr>
            <tr v-bind:rowsudoku="rowsudoku" v-bind:colsudoku="colsudoku"><td @click="putfocus" v-on:mouseover="pintarow" v-on:mouseover="pintacol" v-on:mouseover="pintaBackground" v-on:mouseleave="despintatot">{{valors[0].caselles[3]}}</td><td @click="putfocus" v-on:mouseover="pintarow" v-on:mouseover="pintacol" v-on:mouseover="pintaBackground" v-on:mouseleave="despintatot">{{valors[0].caselles[4]}}</td><td @click="putfocus" v-on:mouseover="pintarow" v-on:mouseover="pintacol" v-on:mouseover="pintaBackground" v-on:mouseleave="despintatot">{{valors[0].caselles[5]}}</td></tr>
            <tr v-bind:rowsudoku="rowsudoku" v-bind:colsudoku="colsudoku"><td @click="putfocus" v-on:mouseover="pintarow" v-on:mouseover="pintacol" v-on:mouseover="pintaBackground" v-on:mouseleave="despintatot">{{valors[0].caselles[6]}}</td><td @click="putfocus"v-on:mouseover="pintarow" v-on:mouseover="pintacol" v-on:mouseover="pintaBackground" v-on:mouseleave="despintatot">{{valors[0].caselles[7]}}</td><td @click="putfocus" v-on:mouseover="pintarow" v-on:mouseover="pintacol" v-on:mouseover="pintaBackground" v-on:mouseleave="despintatot">{{valors[0].caselles[8]}}</td></tr>
        </table>             
    </div>`
});


function iniciar()
{
    app = new Vue({
        el: '#index',
        data: function(){return {puntuacions:{punts:[]}}},
        template: `<div id="punts" style="width:100%;" >                    
                        <h1 style="text-align:center;font-size:3em;">Puntuacions</h1>
                        <transition name="bounce">
                            <label v-if=puntuacions.punts[0] style="text-align:center; width:100%; display:block; font-size:2em;" id="lbPunts1"><b>Primer: </b>{{puntuacions.punts[0].hores}} hores ; {{puntuacions.punts[0].minuts}} minuts ; {{puntuacions.punts[0].segons}} segons </label>
                        </transition>
                        <transition name="bounce">
                            <label v-if=puntuacions.punts[1] style="text-align:center; width:100%; display:block; font-size:2em;" id="lbPunts2"><b>Segon: </b>{{puntuacions.punts[1].hores}} hores ; {{puntuacions.punts[1].minuts}} minuts ; {{puntuacions.punts[1].segons}} segons </label>
                        </transition>
                        <transition name="bounce">
                             <label v-if=puntuacions.punts[2] style="text-align:center; width:100%; display:block; font-size:2em;" id="lbPunts3"><b>Tercer: </b>{{puntuacions.punts[2].hores}} hores ; {{puntuacions.punts[2].minuts}} minuts ; {{puntuacions.punts[2].segons}} segons </label>
                        </transition>
                        <transition name="slide">
                            <button v-if=puntuacions.punts[0] style="margin:auto; margin-top:10%; display:block; font-size:1.5em;" id="btInici" onclick="iniciarJoc(event)">Iniciar el Joc</button>                                                
                        </transition>
                   </div>
        `,
        methods:{
            afegirDades:function(){                
                this.puntuacions.punts.push(JSON.parse(localStorage.getItem("temps1")));
                this.puntuacions.punts.push(JSON.parse(localStorage.getItem("temps2")));
                this.puntuacions.punts.push(JSON.parse(localStorage.getItem("temps3")));                                
            },
        }
    });
    app.afegirDades();
}



function iniciarJoc(event)
{    
    if(event.target.id==="btInici")
    {
        event.target.parentElement.style.display="none";
        //let divtemps = document.getElementById("divtemps");
        divtemps.style.display = "block";
        app.show = true;
    }
    else if(event.target.id ==="btFi"){        
        window.location.reload();
    }

    app = new Vue({
        el: '#app',
        data: {
          show:false,
        },
        template: `
            <div id="contenidor">            
              <areas style="top:8px;" rowsudoku="0" id="area1" colsudoku="0" nom="area1" ref="ar1" ></areas>
              <areas style="left:120px;top:8px;" rowsudoku="0" colsudoku="1" id="area2" nom="area2" ref="ar2"></areas>
              <areas style="left:240px;top:8px;" rowsudoku="0" colsudoku="2" id="area3" nom="area3" ref="ar3"></areas>
              <areas style="left:0px;top:103px;" rowsudoku="1" colsudoku="0" id="area4" nom="area4" ref="ar4"></areas>
              <areas style="left:120px;top:103px;" rowsudoku="1" colsudoku="1" id="area5" nom="area5" ref="ar5"></areas>
              <areas style="left:240px;top:103px;" rowsudoku="1" colsudoku="2" id="area6" nom="area6" ref="ar6"></areas>
              <areas style="left:0px;top:199px;" rowsudoku="2" colsudoku="0" id="area7" nom="area7" ref="ar7"></areas>
              <areas style="left:120px;top:199px;" rowsudoku="2" colsudoku="1" id="area8" nom="area8" ref="ar8"></areas>
              <areas style="left:240px;top:199px;" rowsudoku="2" colsudoku="2" id="area9" nom="area9" ref="ar9"></areas>
              <button style="margin:auto; margin-top:10%; display:block; font-size:1.5em;" id="btFi" onclick="iniciarJoc(event)">Finalitzar</button>
            </div>
            `
      });
      area1 = app.$refs.ar1;
      area2 = app.$refs.ar2;
      area3 = app.$refs.ar3;
      area4 = app.$refs.ar4;
      area5 = app.$refs.ar5;
      area6 = app.$refs.ar6;
      area7 = app.$refs.ar7;
      area8 = app.$refs.ar8;
      area9 = app.$refs.ar9;      
      crearBD();
      let aleat = Math.floor(Math.random() * (dadesSudoku.length));
      omplirTauler(aleat);
      segons = 0;
      minuts = 0;
      hores = 0;
      cronometre = setInterval(afegirSegons,1000);
}

function crearBD()
{
    console.log('crear');
    dadesSudoku = [{//Array d'objectes javascript 
        nom:"sudoku1",
        nums:[[5,3,4,6,7,2,1,9,8],[6,7,8,1,9,5,3,4,2],[9,1,2,3,4,8,5,6,7],
              [8,5,9,4,2,6,7,1,3],[7,6,1,8,5,3,9,2,4],[4,2,3,7,9,1,8,5,6],
              [9,6,1,2,8,7,3,4,5],[5,3,7,4,1,9,2,8,6],[2,8,4,6,3,5,1,7,9]]
        }
        ,
        {
        nom:"sudoku2",
        nums:[[1,3,6,5,9,4,7,2,8],[4,9,2,7,6,8,3,1,5],[5,8,7,1,3,2,9,6,4],
              [8,6,1,2,7,5,3,4,9],[5,7,4,9,3,1,2,8,6],[2,9,3,6,4,8,7,1,5],
              [6,5,7,4,8,2,9,1,3],[1,4,3,6,5,9,8,2,7],[8,2,9,3,7,1,4,5,6]]
        },
        {
            nom:"sudoku3",
            nums:[[7,6,5,8,9,4,2,3,1],[8,3,2,6,7,1,5,9,4],[9,4,1,3,5,2,7,8,6],
                  [6,5,8,9,7,3,1,4,2],[2,1,9,4,8,6,7,5,3],[4,7,3,1,2,5,6,9,8],
                  [4,2,9,5,8,6,3,1,7],[1,6,8,3,4,7,9,2,5],[5,3,7,2,1,9,8,6,4]]
        },
    ];
        
       
        

        const DB_VERSION = 23;
        window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
        window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction || {
        READ_WRITE: "readwrite"
        };
        window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
        var peticioObertura = window.indexedDB.open("Sudokus", DB_VERSION);
        
        peticioObertura.onerror = function(event) {
        alert("Problema!");
        };
        var db;
        
        peticioObertura.onsuccess = function(event) {
        db = event.target.result;
        };

        peticioObertura.onupgradeneeded = function(event) {
            var db = event.target.result;
            try
            {
            db.deleteObjectStore("dadesSudoku2");
            }
            catch(e)
            {}
            //var magatzemSudoku = db.createObjectStore("dadesSudoku2", {
            var magatzemSudoku = db.createObjectStore("dadesSudoku2", {
                keyPath:'nom'
            });
           
            magatzemSudoku.transaction.oncomplete = function(event) {
            var magatzemSudoku = db.transaction("dadesSudoku2", "readwrite").objectStore("dadesSudoku2");
                    console.log("Entra");
                    var peticio = magatzemSudoku.add(dadesSudoku[0]);
                    peticio.onsuccess = () => console.log('Sudoku 1 desat.');
                    var peticio = magatzemSudoku.add(dadesSudoku[1]);
                    peticio.onsuccess = () => console.log('Sudoku 2 desat.');
                    var peticio = magatzemSudoku.add(dadesSudoku[2]);
                    peticio.onsuccess = () => console.log('Sudoku 3 desat.');
        }  
    }    
}

////////////////////////////////////////
//
//  Omple el tauler amb el sudoku desat a la array de sudokus desada
//  a la base de dades indexDB que te el index passat com a argument
//
///////////////////////////////////////////
//
function omplirTauler(numsudoku)
{
    console.log("Entra a omplirTauler");
    var peticioObertura = window.indexedDB.open("Sudokus", 23);
    console.log("Base dades: " + peticioObertura);
    var db;

    peticioObertura.onsuccess = function (event) {
        console.log("obertura a omplirTauler ok")
        db = peticioObertura.result;
        var datos = db.transaction('dadesSudoku2',"readwrite");
        var object = datos.objectStore('dadesSudoku2');
        var elements = [];

        object.openCursor().onsuccess = function(e){
            var result = e.target.result;
            if(result === null)
                return;
            elements.push(result.value);
            result.continue();
        }

        console.log("Obertura ok" + db)
        datos.oncomplete = function(){
            app.$refs.ar1.afegirDades((elements[numsudoku].nums)[0]);
            app.$refs.ar1.eliminarRandom();
            app.$refs.ar2.afegirDades((elements[numsudoku].nums)[1]);
            app.$refs.ar2.eliminarRandom();
            app.$refs.ar3.afegirDades((elements[numsudoku].nums)[2]);
            app.$refs.ar3.eliminarRandom();
            app.$refs.ar4.afegirDades((elements[numsudoku].nums)[3]);
            app.$refs.ar4.eliminarRandom();
            app.$refs.ar5.afegirDades((elements[numsudoku].nums)[4]);
            app.$refs.ar5.eliminarRandom();
            app.$refs.ar6.afegirDades((elements[numsudoku].nums)[5]);
            app.$refs.ar6.eliminarRandom();
            app.$refs.ar7.afegirDades((elements[numsudoku].nums)[6]);
            app.$refs.ar7.eliminarRandom();
            app.$refs.ar8.afegirDades((elements[numsudoku].nums)[7]);
            app.$refs.ar8.eliminarRandom();
            app.$refs.ar9.afegirDades((elements[numsudoku].nums)[8]);
            app.$refs.ar9.eliminarRandom();
            //elements.forEach(x => console.log("Dato: " + x.nums));
        }
    }; 
}

function afegirSegons()
{
    segons++;
    if(segons > 60)
    {
        minuts++;
        segons = 0;
    }
    if(minuts > 60)
    {
        hores++;
        minuts = 0;
    }
    let lbs = document.getElementById("lbSegons");
    let lbm = document.getElementById("lbMinuts");
    let lbh = document.getElementById("lbHores");
    lbs.innerText = ": " + segons;
    lbm.innerText = ": " + minuts;
    lbh.innerText = hores;
}

